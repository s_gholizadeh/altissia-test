package org.altissia.test.fizzbuzz;

import org.altissia.test.fizzbuzz.exception.BadRequestException;
import org.altissia.test.fizzbuzz.service.FizzBuzzService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
class FizzBuzzApplicationTests {

    @Autowired
    private FizzBuzzService service;

    @Test
    void testFizzBuzz() {
        List<String> numbers1 = Arrays.asList("Fizz");
        List<String> numbers2 = Arrays.asList("Buzz");
        List<String> numbers3 = Arrays.asList("FizzBuzz");
        List<String> numbers4 = Arrays.asList("2");

        Assertions.assertIterableEquals(service.fizzbuzz(3).getResult(), numbers1);
        Assertions.assertIterableEquals(service.fizzbuzz(10).getResult(), numbers2);
        Assertions.assertIterableEquals(service.fizzbuzz(30).getResult(), numbers3);
        Assertions.assertIterableEquals(service.fizzbuzz(2).getResult(), numbers4);


        List<Integer> numbers5 = Arrays.asList(1,30,4,3,5,10);
        List<String> numbers6 = Arrays.asList("1", "FizzBuzz", "4", "Fizz", "Buzz", "Buzz");
        Assertions.assertIterableEquals(service.fizzbuzz(numbers5).getResult(), numbers6);


        List<Integer> numbers7 = Arrays.asList(1,30,null);
        Throwable exception = assertThrows(BadRequestException.class, ()-> service.fizzbuzz(numbers7));
    }

    @Test
    void testFizzBuzzBazz(){
        String numbers1 = "Fizz";
        String numbers2 = "Buzz";
        String numbers3 = "Bazz";
        String numbers4 = "FizzBuzz";
        String numbers5 = "FizzBazz";
        String numbers6 = "BuzzBazz";
        String numbers7 = "FizzBuzzBazz";
        String numbers8 = "2";

        Assertions.assertEquals(service.newConvert(18), numbers1);
        Assertions.assertEquals(service.newConvert(10), numbers2);
        Assertions.assertEquals(service.newConvert(14), numbers3);
        Assertions.assertEquals(service.newConvert(30), numbers4);
        Assertions.assertEquals(service.newConvert(42), numbers5);
        Assertions.assertEquals(service.newConvert(70), numbers6);
        Assertions.assertEquals(service.newConvert(315), numbers7);
        Assertions.assertEquals(service.newConvert(2), numbers8);

    }


}
