package org.altissia.test.fizzbuzz.service;

import org.altissia.test.fizzbuzz.dto.MultiResult;

import java.util.List;

public interface FizzBuzzService {
    /**
     * If there is no entry or it's empty, the output must be an array with all the answers from 1 to 100
     */
    MultiResult fizzbuzz();

    MultiResult fizzbuzz(Integer... entry);

    MultiResult fizzbuzz(List<Integer> entries);

    String newConvert(Integer number);
}