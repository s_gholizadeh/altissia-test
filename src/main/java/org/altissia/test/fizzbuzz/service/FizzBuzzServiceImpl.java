package org.altissia.test.fizzbuzz.service;

import org.altissia.test.fizzbuzz.dto.MultiResult;
import org.altissia.test.fizzbuzz.exception.BadRequestException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class FizzBuzzServiceImpl implements FizzBuzzService {

    /**
     * That endpoint will apply the fizzbuzz output with the following rules:
     *  - If the entry is a multiple of 3, the output must be "fizz"
     *  - If the entry is a multiple of 5, the output must be "buzz"
     *  - If the entry is a multiple of 3 and 5, the output must be "fizzbuzz"
     *  - Otherwise the output must be the entry
     */
    private String convert(Integer number) {
        if(Objects.isNull(number))
            throw new BadRequestException();
        return (number %3 == 0) ? (number %5 == 0 ? "FizzBuzz" : "Fizz") : ( number %5 == 0 ? "Buzz" : number.toString());
    }

    @Override
    public MultiResult fizzbuzz(List<Integer> entries) {
        return new MultiResult(entries.stream().map(this::convert).collect(Collectors.toList()));
    }

    @Override
    public MultiResult fizzbuzz() {
        return fizzbuzz(IntStream.rangeClosed(1, 100).boxed().collect(Collectors.toList()));
    }

    @Override
    public MultiResult fizzbuzz(Integer... entry) {
        return fizzbuzz(Arrays.asList(entry));
    }

    public String newConvert(Integer number) {
        String fizzBazz = convert(number);
        return (number %7 == 0) ? (StringUtils.isNumeric(fizzBazz) ? "Bazz" : fizzBazz + "Bazz") : fizzBazz;
    }
}
