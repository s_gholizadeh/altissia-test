package org.altissia.test.fizzbuzz.controller;

import lombok.RequiredArgsConstructor;
import org.altissia.test.fizzbuzz.dto.MultiResult;
import org.altissia.test.fizzbuzz.service.FizzBuzzService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/fizzbuzz")
public class FizzBuzz {

    private final FizzBuzzService fizzBuzzService;

    /**
     * Endpoint: /fizzbuzz?entry=[int]
     * The output is formatted in JSON
     * An error is thrown if the entry is not a Int
     * If there is no entry or it's empty, the output must be an array with all the answers from 1 to 100
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public MultiResult fizzbuzz(@RequestParam(required = false) Integer entry){
        if(Objects.isNull(entry))
            return fizzBuzzService.fizzbuzz();
        return fizzBuzzService.fizzbuzz(entry);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public MultiResult fizzbuzz(@RequestBody List<Integer> entries){
        return fizzBuzzService.fizzbuzz(entries);
    }
}
